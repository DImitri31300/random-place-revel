(function(d) {
    /* method 1 : precodural */
    var result = d.getElementById('result')

    d.getElementById('make-group').addEventListener('submit', function(e) {
        let groups = [],
            restStudents = students.slice(),
            i = 0

        // generate group
        while(restStudents.length) {
            let index = Math.floor(Math.random() * restStudents.length),
                indexGroup = i%parseFloat(d.getElementById('nb-group').value) + 1

            // Create array of group if not exist
            if(!groups[indexGroup]) {
                groups[indexGroup] = []
            }

            // add studient in target group
            groups[indexGroup].push(restStudents[index])

            // delete studient of rest
            restStudents.splice(index, 1)

            i++
        }

        // show group
        result.innerHTML = '';
        groups.forEach(function(group, index) {
            let newGroup = d.createElement('ul')

            group.forEach(function(element) {
                let newItem = d.createElement('li')
                
                newItem.appendChild(d.createTextNode(element))
    
                newGroup.appendChild(newItem)
            });
    
            result.appendChild(newGroup)
        })

        //Stop propagation
        e.preventDefault()
    });

  
   var students = [
            "Mary",
            "Lucas",
            "Ridhoine",
            "Maxime",
            "DIMITRI",
            "Thomas",
            "Maréa",
            "Elodie",
            "Abdellah",
            "CAROLINE",
            "Emma",
            "Cathy",
            "Boris",
            "MYLENE",
            "Cécile"
        ];

function updateNumberList(array){
  var s="";
  for(var i=0;i<array.length;i++){
    s+="<br>"+array[i];
  }
  document.getElementById("numberList").innerHTML=s;
}

function shuffleUsingRandomSwapping(array){
  var j,x,i=0,len=array.length;
  for(i;i<len;i++){
    j=Math.floor(Math.random()*len);
    x=array[i];
    array[i]=array[j];
    array[j]=x;
  }
}

function shuffleUsingSortFunc(array){
  array.sort(function(){
    return 0.5-Math.random();
  });
}
    
})(document);
